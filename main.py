from automation_class import *

def main():
    obj1 = TestNykaa()

    driver = obj1.initialise_driver()

    # obj1.register(driver)
    # time.sleep(20)

    obj1.login(driver)
    time.sleep(20)

    obj1.scroll(driver)
    time.sleep(2)

    obj1.search_product(driver)
    time.sleep(2)

    obj1.add_to_cart(driver)
    time.sleep(3)

    obj1.buy_product(driver)
    time.sleep(5)


if __name__ == "__main__":
    main()
