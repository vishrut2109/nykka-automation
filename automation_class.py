import time

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By

from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys


class TestNykaa():
    def initialise_driver(self):
        ops = webdriver.ChromeOptions()
        ops.add_argument("--disable-notifications")

        serv_obj = Service("/Users/vishrutsharma/Desktop/drivers/chromedriver_mac_arm64/chromedriver")
        driver = webdriver.Chrome(service=serv_obj)
        driver.implicitly_wait(10)

        driver.get('https://www.nykaa.com/')
        driver.maximize_window()
        time.sleep(5)
        return driver

    def register(self, driver):
        sign_in_button = driver.find_element(By.XPATH, "//button[normalize-space()='Sign in with Mobile / Email']")
        sign_in_button.click()

        # put temp email in send_keys below
        driver.find_element(By.XPATH, "//input[@placeholder='Enter Email ID or Phone Number']").send_keys(
            "marex78657@djpich.com")
        driver.find_element(By.XPATH, "//button[@id='submitVerification']").click()
        driver.find_element(By.XPATH, "//input[@id='userName']").send_keys("testUser")
        driver.find_element(By.XPATH, '//input[@placeholder="Phone Number "]').send_keys("9459860285")
        driver.find_element(By.XPATH, '//button[@class="button big fill full mt60 false"]').click()


    def login(self,driver):
        sign_in_button = driver.find_element(By.XPATH, "//button[normalize-space()='Sign in with Mobile / Email']")
        sign_in_button.click()

        driver.find_element(By.XPATH, "//input[@placeholder='Enter Email ID or Phone Number']").send_keys("9459860286")
        driver.find_element(By.XPATH, "//button[@id='submitVerification']").click()


    def scroll(self,driver):
        actions = ActionChains(driver)
        element = driver.find_element(By.XPATH, "//img[@alt='Easy Returns']")
        actions.move_to_element(element).perform()


    def search_product(self,driver):
        search_box = driver.find_element(By.XPATH, "//input[@placeholder='Search on Nykaa']")
        search_box.send_keys("Sports Shoes", Keys.ENTER)
        driver.find_element(By.XPATH,
                            "// img[ @ alt = 'Puma Ultra Play Indoor Turf Junior Kids Orange Football Shoes']").click()


    def add_to_cart(self,driver):
        # window id of sports shoes search page and nykka home page is same as they are in same window but titles are
        # different
        window_id_search_page = driver.current_window_handle
        print(driver.title, window_id_search_page)

        window_ids = driver.window_handles
        window_id_sports_shoes = window_ids[1]

        driver.switch_to.window(window_id_sports_shoes)
        print(driver.title, window_id_sports_shoes)

        add_to_cart_button = driver.find_element(By.XPATH,
                                                 "//div[@class='css-vp18r8']//span[@class='btn-text'][normalize-space()='Add to Bag']")
        add_to_cart_button.click()

        action = ActionChains(driver)
        cart_button = driver.find_element(By.XPATH, "//button[@class='css-g4vs13']//*[name()='svg']")

        # Move to the add to cart button and click on it
        action.move_to_element(cart_button).click().perform()


    def buy_product(self,driver):
        driver.switch_to.frame(0)

        # click proceed button
        driver.find_element(By.XPATH, "//span[@class=' css-1l4d0ex e171rb9k3']").click()

        # click deliver here button
        driver.find_element(By.XPATH, "//button[normalize-space()='Deliver here']").click()

        # click cash on delivery
        cod_button = driver.find_element(By.XPATH, "//p[normalize-space()='Cash on delivery']")
        cod_button.click()

        # click place order button , uncomment the line below to actually place the order
        # driver.find_element(By.XPATH,"//button[normalize-space()='Place order']").click()



